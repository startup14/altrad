import axios from 'axios';

export default {
    state: {
        isLogin: false,
    },
    getters: {
        IS_LOGIN(state) {
            return state.isLogin
        }
    },
    mutations: {
        setToken(state, token) {
            localStorage.setItem(
                "access_token",
                token
            );
            axios
                .defaults
                .headers
                .common['Authorization'] = `Bearer ${token}`;
            state.isLogin = true;
        },
        dropToken(state) {
            state.isLogin = false;
            localStorage.removeItem("access_token");
            axios
                .defaults
                .headers
                .common['Authorization'] = null;
        },
    },
    actions: {
        LOGIN({ commit }, data) {
            axios.post('/auth/login', data).then(response => {
                commit('setToken', response.data.access_token);
            });
        },
        LOGOUT({commit}) {
            axios.delete('/auth/logout').then(() => {
                commit('dropToken');
            })
        },
        SET_TOKEN({ commit }, token) {
            commit('setToken', token);
        }
    },
}
