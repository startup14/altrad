<?php

namespace App\Console\Watchers;

use App\Http\Controllers\WebsocketController;

interface ChangeChecker
{
    public function check(WebsocketController $controller, array $prev): array;
    public function justUpdates(WebsocketController $controller, array $prev): array;
}
