<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use React\EventLoop\Loop;
use Spatie\ShortSchedule\ShortSchedule;

class ShortScheduleRunCommand extends Command
{
    protected $signature = 'short-schedule:run {--lifetime= : The lifetime in seconds of worker}';

    protected $description = 'Run the short scheduled commands';

    public function handle(): int
    {
        $loop = Loop::get();
        (new ShortSchedule($loop))->registerCommands()->run($this->option('lifetime'));
        return 0;
    }
}
