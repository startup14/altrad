<?php

namespace App\QuoteProviders\CryptoCompare;

use Illuminate\Support\Facades\Cache;

use React\EventLoop;
use Ratchet\Client as Ratchet;

use Ratchet\RFC6455\Messaging;

use Ratchet\RFC6455\Messaging\FrameInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;

use App\QuoteProviders\BaseWebSocket;

class WebSocket extends BaseWebSocket
{
    /**
     * @var string
     */
    protected string $endpoint = "wss://streamer.cryptocompare.com/v2";

    /**
     * @var string
     */
    protected string $stream;

    /**
     * @var array
     */
    private array $store;

    /**
     * @var array
     */
    private array $pairs;

    public function __construct(array &$template, callable $onMessage)
    {
        $this->endpoint .= "?api_key=" . env('CRYPTO_COMPARE_API_KEY');

        parent::__construct(function (array $message) use ($onMessage) {
            if (isset($message['TYPE']) && ($message['TYPE'] == '2')) {
                $answer = $this->answerDecoration($message);
                $onMessage($answer);
            }
        }, [
            [
                'action' => 'SubAdd',
                'subs' => function () use (&$template) {
                    $subscribes = [];
                    foreach ($template as $baseSymbol => $base) {
                        foreach (array_keys($base['quotes']) as $quoteSymbol) {
                            $subscribes[] = "2~Binance~{$baseSymbol}~{$quoteSymbol}";
                        }
                    }
                    return $subscribes;
                }
            ],
        ]);
    }

    private function answerDecoration(array $message): array
    {
        return [
            'base' => $message['FROMSYMBOL'] ?? null,
            'quote'  => $message['TOSYMBOL'] ?? null,
            'open'  => $message['OPENDAY'] ?? null,
            'close' => $message['PRICE'] ?? null,
            'high'  => $message['HIGHDAY'] ?? null,
            'low'   => $message['LOWDAY'] ?? null,
            'vol'   => $message['VOLUMEDAY'] ?? null,
            'open_hour' => $message['OPENHOUR'] ?? null,
            'high_hour' => $message['HIGHHOUR'] ?? null,
            'low_hour'  => $message['LOWHOUR'] ?? null,
            'vol_hour'  => $message['VOLUMEHOUR'] ?? null,
        ];
    }
}
