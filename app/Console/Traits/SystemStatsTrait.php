<?php

namespace App\Console\Traits;

trait SystemStatsTrait
{
    protected int $mem_usage;

    protected function memoryStatLine(): string
    {
        $before = $this->mem_usage;
        $after = $this->mem_usage = memory_get_usage(true);
        $diff = $after - $before;
        return sprintf('Memory usage: %s, diff: %s', human_readable($after), human_readable($diff));
    }
}
