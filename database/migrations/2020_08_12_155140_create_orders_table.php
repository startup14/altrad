<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('status')->default('open');
            $table->string('class');
            $table->string('type');
            $table->double('amount');
            $table->double('remaining')->nullable();
            $table->double('limit');
            $table->double('value');
            $table->string('pair');
            $table->string('currency');
            $table->double('liq');
            $table->double('lv');
            $table->integer('user_id');
            $table->double('leverage');
            $table->double('take_profit')->nullable();
            $table->double('stop_loss')->nullable();
            $table->string('type_of_market');
            $table->string('unit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
