<?php

namespace App\QuoteProviders;

use App\Logger;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response as HttpResponse;

abstract class BaseAPI
{
    public function __call(string $name, array $args): self
    {
        $this->queryParams[$name] = $args[0];

        return $this;
    }

    /**
     * @param string $path
     * @param array $queryParams
     * @return HttpResponse|null
     */
    protected function send(string $path, array $queryParams = []): ?HttpResponse
    {
        $endpoint = "{$this->endpoint}/{$path}";

        $params = array_merge($this->queryParams, $queryParams);

        if (count($params)) {

            $query = implode('&',
                array_map(function ($key, $value) {
                    return "{$key}={$value}";
                }, array_keys($params), array_values($params))
            );
            $endpoint .= "?{$query}";
        }

        try {
            return Http::get($endpoint);
        } catch (\Exception $exception) {
            Logger::create([
                'description' => "Request error - {$endpoint}"
            ]);
        }
        return null;
    }
}
