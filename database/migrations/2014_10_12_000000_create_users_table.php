<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('email')->unique();
            $table->string('country_name')->nullable();
            $table->string('country_code')->nullable();
            $table->string('ip')->nullable();
            $table->string('role')->default('trader');
            $table->double('balance')->default(0);
            $table->double('bonuses')->default(0);
            $table->double('withdraw_money')->default(0);
            $table->enum('is_active', ['0','1'])->default(1);
            $table->string('account_status')->default('can_trade');
            $table->boolean('credit_log_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->longText('bio')->nullable();
            $table->integer('auth_rating')->default(40);
            $table->string('tw')->nullable();
            $table->string('fb')->nullable();
            $table->boolean('exp_in_trade')->default(0);
            $table->integer('rating_percent')->nullable();
            $table->integer('level')->default(1);
            $table->string('lang')->default('eng');
            $table->double('crypto_fee')->default('0.001');
            $table->double('stock_fee')->default('0.001');
            $table->double('positions_fee')->default('0.001');
            $table->double('commodities_fee')->default('0.001');
            $table->double('min_deal')->default(100);
            $table->double('commissions_analytics')->nullable();
            $table->double('commissions_trade')->nullable();
            $table->double('swop')->default(0);
            $table->string('avatar')->nullable();
            $table->binary('id_front')->nullable();
            $table->binary('id_back')->nullable();
            $table->binary('card_front')->nullable();
            $table->binary('card_back')->nullable();
            $table->binary('ub')->nullable();
            $table->binary('dod')->nullable();
            $table->unsignedBigInteger('follow')->nullable();
            $table->enum('is_blocked', ['0', '1'])->default('0');
            $table->string('subscription')->default('silver');
            $table->enum('online', ['0', '1'])->default('0');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
