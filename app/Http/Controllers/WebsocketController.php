<?php

namespace App\Http\Controllers;

use App\ActiveConnection;
use App\Console\Traits\SmartOutput;
use App\Console\Traits\SystemStatsTrait;
use App\Orders;
use App\OrdersHistory;
use App\Positions;
use App\User;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use LogicException;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use RuntimeException;
use SplObjectStorage;
use SplQueue;

class WebsocketController extends Controller implements MessageComponentInterface
{
    use SystemStatsTrait;
    use SmartOutput;

    public const USER_ONLINE = '1';
    public const USER_OFFLINE = '0';
    public const USER_FRESH_SEC = 4;

    public SplObjectStorage $clients;
    public SplObjectStorage $charts;

    private string $signature = 'websocket:controller';

    private array $active_users;

    public function __construct()
    {
        $this->clients = new SplObjectStorage();
        $this->charts = new SplObjectStorage();
        $this->mem_usage = memory_get_usage(true);
        $this->active_users = [];
    }

    public function activeUsers(): array
    {
        return $this->active_users;
    }

    private function addActiveUser(int $user_id): array
    {
        if (! in_array($user_id, $this->active_users, true)) {
            $this->active_users[] = $user_id;
        }
        return $this->active_users;
    }

    public function removeActiveUser(int $user_id): array
    {
        if (($key = array_search($user_id ,$this->active_users, true)) !== false) {
            unset($this->active_users[$key]);
        }
        return $this->active_users;
    }

    protected function line(string $line): void
    {
        echo $this->smartLineCrlf($line);
    }

    public function onOpen(ConnectionInterface $conn): int
    {
        return $this->updateOnlineDeviceCount();
    }

    protected function updateOnlineDeviceCount(): int
    {
        $online_count = $this->clients->count();
        Cache::put('device', $online_count);
        $this->line("Total clients ATTACHED: $online_count");
        return $online_count;
    }

    private function updateConnectionStatus(int $user_id, string $activity_token, string $status): int
    {
        if ($status) {
            ActiveConnection::create(compact('user_id', 'activity_token'));
            return $status;
        }

        ActiveConnection::where(compact('user_id', 'activity_token'))->delete();
        return $status;
    }

    public function onClose(ConnectionInterface $conn): int
    {
        $this->setClientOffline($conn);
        return $this->updateOnlineDeviceCount();
    }

    public function onError(ConnectionInterface $conn, Exception $e): int
    {
        $chats = [
            '1301684693',
            '1369422538'
        ];

        try {
            if (!str_contains($e->getFile(), 'AbstractRouteCollection')) {
                foreach ($chats as $chat) {
                    Http::post(
                        "https://api.telegram.org/bot1382575312:AAHFzNjaVKKUXHzMlzjrfG83yOtN_SlzDRE/sendMessage?chat_id=$chat&text=WS, "
                        . $e->getMessage() . ", file: " . $e->getFile() . ", line: " . $e->getLine());
                }
            }
        } finally {
            $this->setClientOffline($conn);
            $conn->close();
            return $this->updateOnlineDeviceCount();
        }
    }

    private function initUserFrom(ConnectionInterface $conn, array $msg): array
    {
        return [
            'conn' => $conn,
            'user_id' => $msg['id'],
            'type' => 'crypto',
            'currency' => 'BTC',
            'pair' => 'USDT',
            'activity_token' => Str::random(25),
            'session_id' => $msg['session_id'] ?? null,
            'remote_ip' => $conn->remoteAddress,
        ];
    }

    private function setUserConnectionStatus(int $user_id, string $online): bool
    {
        return User::whereId($user_id)->update(compact('online'));
    }

    private function setUserOffline(int $user_id): void
    {
        $this->setUserConnectionStatus($user_id, self::USER_OFFLINE);
    }

    private function setUserOnline(int $user_id): void
    {
        $this->setUserConnectionStatus($user_id, self::USER_ONLINE);
    }

    private function addActiveConnection(array $user): void
    {
        $this->updateConnectionStatus($user['user_id'], $user['activity_token'], self::USER_ONLINE);
    }

    /**
     * @throws \JsonException
     */
    private function connectionSend(ConnectionInterface $conn, array $data): void
    {
        $json = json_encode($data, JSON_THROW_ON_ERROR);
        $conn->send($json);
    }

    private function captureUserSettings(int $user_id): array
    {
        $settings = [];

        try {
            $user = User::whereId($user_id)
                ->select('id', 'crypto_fee', 'stock_fee', 'commodities_fee', 'positions_fee', 'balance', 'account_status', 'is_active', 'min_deal', 'min_deal_stock')
                ->first();

            $fees = [
                'crypto_fee' => $user->crypto_fee,
                'stock_fee' => $user->stock_fee,
                'commodities_fee' => $user->commodities_fee,
                'positions_fee' => $user->positions_fee
            ];

            $settings = [
                'fees' => $fees,
                'balance' => $user->balance,
                'account_status' => $user->account_status,
                'is_active' => $user->is_active,
                'min_deal' => $user->min_deal,
                'min_deal_stock' => $user->min_deal_stock
            ];

        } catch (Exception $e) {
            $error = $e->getMessage();
            Log::error($error);
            $settings['error'] = $error;
        }

        return $settings;
    }

    private function captureDbData(int $user_id): array
    {
        $data = [];

        try {
            $data['history'] = OrdersHistory::whereUserId($user_id)->orderBy('created_at')->get();
            $data['positions'] = Positions::whereUserId($user_id)->orderBy('created_at')->get();
            $data['orders'] = Orders::whereUserId($user_id)->orderBy('created_at')->get();
        } catch (Exception $e) {
            $error = $e->getMessage();
            Log::error($error);
            $data['error'] = $error;
        }

        return $data;
    }

    protected function handleUserConnection($msg, ConnectionInterface $conn): bool
    {
        $response = true;

        try {
            $user_id = $msg['id'];
            $user = $this->initUserFrom($conn, $msg);

            $this->setUserOnline($user_id);
            $this->addActiveConnection($user);
            $this->line("Client id [$user_id] DB status updated to ONLINE");

            $this->clients->attach($conn, $user);
            $this->line("Client id [$user_id] ATTACHED");
            $this->addActiveUser($user['user_id']);
            $this->line("Client id [$user_id] set ACTIVE.");

            // 1 send
            $this->connectionSend($conn, ['activity_token' => $user['activity_token']]);
            $this->line("Client id [$user_id] SENT token");

            // 2 send
            $user_setting = $this->captureUserSettings($user_id);
            if ($user_setting) {
                $this->connectionSend($conn, $user_setting);
                $this->line("Client id [$user_id] SENT options");
                $this->line($this->memoryStatLine());
            }

            // 3 send
            $db_data = $this->captureDbData($user_id);
            if ($db_data) {
                $this->connectionSend($conn, $db_data);
                $this->line("Client id $user_id SENT orders, history, etc.");
                $this->line($this->memoryStatLine());
            }

            $this->updateOnlineDeviceCount();

        } catch (Exception $e) {
            $error = $e->getMessage();
            Log::error($error);
            $this->line("ERR: Init sent $error");
            $response = false;
        }

        return $response;
    }

    protected function setClientOffline(ConnectionInterface $connection): int
    {
        $response = 200;

        try {
            if (isset($this->clients[$connection])) {
                $user = $this->clients[$connection];
                $user_id = $user['user_id'];

                $this->updateConnectionStatus($user_id, $user['activity_token'], self::USER_OFFLINE);
                $this->setUserOffline($user_id);
                $this->line("Client id [$user_id] DB Status updated to OFFLINE");

                $this->clients->detach($connection);
                $this->line('Client id [$user_id] DETACHED');

                $this->removeActiveUser($user['user_id']);
                $this->line("Client id [$user_id] set NON_ACTIVE.");

                $connection->close();
                $this->line(sprintf('Client id [%s] CLOSED: [%s]', $user['user_id'], $connection->remoteAddress));

            } else {
                $this->charts->detach($connection);
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
            $response = 500;
        }

        return $response;
    }

    protected function amendClientFrom(array $msg, ConnectionInterface $from): bool
    {
        $this->clients[$from] = [
            'conn' => $this->clients[$from]['conn'],
            'user_id' => $this->clients[$from]['user_id'],
            'type' => $msg['type'],
            'currency' => $msg['currency'],
            'pair' => $msg['pair'] ?? $this->clients[$from]['pair'],
            'activity_token' => $this->clients[$from]['activity_token'],
            'session_id' => $this->clients[$from]['session_id']
        ];
        return true;
    }

    protected function attachChart(ConnectionInterface $from): bool
    {
        $chart = [
            'conn' => $from,
            'interval' => '1d',
            'symbol' => 'BTCUSDT'
        ];

        $response = true;
        try {
            $this->charts->attach($from, $chart);
        } catch (Exception | RuntimeException $e) {
            Log::error($e->getMessage());
            $response = false;
        }

        return $response;
    }

    protected function amendChart(array $msg, ConnectionInterface $from): bool
    {
        $response = true;

        try {
            $this->charts[$from] = [
                'conn' => $from,
                'interval' => $msg['interval'] ?? '1d',
                'symbol' => $msg['symbol']
            ];
        } catch (Exception | LogicException | RuntimeException $e) {
            Log::error($e->getMessage());
            $response = false;
        }

        return $response;
    }

    public function onMessage(ConnectionInterface $from, $msg): bool
    {
        $msg = json_decode($msg, true, 512, JSON_THROW_ON_ERROR);

        // todo: extract if criteria to methods

        if (array_key_exists('id', $msg) && !is_null($msg['id'])) {
            return $this->handleUserConnection($msg, $from);
        }

        if (array_key_exists('type', $msg) && array_key_exists('currency', $msg) && !is_null($this->clients[$from])) {
            return $this->amendClientFrom($msg, $from);
        }

        if (array_key_exists('chart', $msg)) {
            return $this->attachChart($from);
        }

        if (array_key_exists('symbol', $msg)) {
            $this->amendChart($msg, $from);
        }

        return true;
    }
}
