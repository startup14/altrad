<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionsSoftdeleteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('positions_softdelete', function (Blueprint $table) {
            $table->id();
            $table->string('status')->default('open');
            $table->string('class');
            $table->double('entry_price')->nullable();
            $table->double('init_price')->nullable();
            $table->string('type');
            $table->double('amount');
            $table->double('limit');
            $table->double('value');
            $table->string('pair');
            $table->string('currency');
            $table->double('liq');
            $table->double('lv');
            $table->integer('user_id');
            $table->double('leverage');
            $table->double('take_profit')->nullable();
            $table->double('stop_loss')->nullable();
            $table->string('type_of_market');
            $table->string('unit');
            $table->double('swop')->default(0);
            $table->double('balance_before');
            $table->double('balance_after');
            $table->double('crypto_amount')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('positions_softdelete');
    }
}
