<?php

namespace App\Console\Commands;


class CommoUpdates extends AbstractUpdates
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commo:updates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Spam stock updates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->keyspace = REDIS_STREAM_COMMO;
        parent::__construct();
    }

    protected function clientUpdatesFilter(int $user_id): array
    {
        return [];
    }

    protected function clientUpdates(int $user_id, array $cache): array
    {
        $filter = $this->clientUpdatesFilter($user_id);
        return fallback_commodities($cache, $filter);
    }

    protected function updatesInCache(): array
    {
        $updated_keys = redis_sget($this->update_key);
        return redis_hgetall_for_keys($updated_keys);
    }

    protected function needPrecache(): bool
    {
        return true;
    }
}
