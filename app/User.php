<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'country_name',
        'country_code',
        'ip',
        'subscription',
        'is_investor',
        'is_trader',
        'balance',
        'is_mining',
        'credit_log_id',
        'password',
        'crypto_fee',
        'stock_fee',
        'positions_fee',
        'commodities_fee',
        'id_front',
        'id_back',
        'card_front',
        'card_back',
        'ub',
        'dod',
        'is_active',
        'withdraw_money',
        'min_deal',
        'avatar',
        'account_status',
        'auth_rating',
        'follow',
        'bitcoin_wallet',
        'online',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
