<?php

namespace App\QuoteProviders\TwelveData;

use Illuminate\Support\Facades\Cache;

use React\EventLoop;
use Ratchet\Client as Ratchet;

use Ratchet\RFC6455\Messaging;

use Ratchet\RFC6455\Messaging\FrameInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;

use App\QuoteProviders\BaseWebSocket;

class WebSocket extends BaseWebSocket
{
    /**
     * @var string
     */
    protected string $endpoint = "wss://ws.twelvedata.com/v1/quotes/price";

    /**
     * @var string
     */
    protected string $stream;

    /**
     * @var array
     */
    private array $store;

    /**
     * @var array
     */
    private array $pairs;

    public function __construct(array &$template, callable $onMessage)
    {
        $this->endpoint .= "?apikey=" . env('TWELVE_API_KEY');

        parent::__construct(function (array $message) use ($onMessage) {
            $answer = $this->answerDecoration($message);
            $onMessage($answer);
        }, [[
            'action' => 'subscribe',
            'params' => [
                'symbols' => implode(',', array_keys($template)),
            ]
        ]]);
    }

    private function answerDecoration(array $message = []): array
    {
        if ($message['event'] === 'price') {
            $message['close'] = (double)$message['price'];
            unset($message['price']);
        }
        if (isset($message['day_volume'])) {
            $message['vol'] = $message['day_volume'];
            unset($message['day_volume']);
        }
         return $message;
    }
}
