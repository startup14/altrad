<?php

namespace App\Console\Commands;

use App\Console\Traits\SmartOutput;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use JsonException;

abstract class AbstractUpdates extends Command
{
    use SmartOutput;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'abstract:updates';
    protected $description = 'Spam abstract updates';

    protected string $keyspace;
    protected string $update_key;

    public function __construct()
    {
        parent::__construct();
        $this->update_key = $this->updateKey();
    }

    protected function updateKey(): string
    {
        return redis_create_update_key($this->keyspace);
    }

    protected function userDataKey(int $user_id): string
    {
        return redis_create_keyspace_user_key($this->keyspace, $user_id);
    }

    protected function clearUpdates(): void
    {
        if (Redis::unlink($this->update_key)) {
            $this->infoWithTime("Redis updates cleared");
        }
    }

    protected function activeUsers(): array
    {
        return redis_sget(REDIS_ACTIVE_USERS);
    }

    abstract protected function clientUpdatesFilter(int $user_id): array;
    abstract protected function clientUpdates(int $user_id, array $cache): array;
    abstract protected function needPrecache(): bool;
    abstract protected function updatesInCache(): array;

    public function handle(): int
    {
        $this->infoWithTime("Start looking for $this->keyspace updates...");

        $updates_in_cache = $this->updatesInCache();
        if ( (! $updates_in_cache) && $this->needPrecache()) {
            $this->infoWithTime("No required precache found for $this->keyspace. Continue.");
            return 0;
        }

        $this->infoWithTime(sprintf('Updates in precache: %d pcs', count($updates_in_cache)));

        $active_users = $this->activeUsers();
        if ($active_users) {
            foreach ($this->activeUsers() as $user_id) {
                $client_updates = $this->clientUpdates($user_id, $updates_in_cache);
                if ($client_updates) {
                    $client_updates_key = $this->userDataKey($user_id);
                    try {
                        $client_json = json_encode($client_updates, JSON_THROW_ON_ERROR);
                        redis_set($client_updates_key, $client_json);
                        $this->infoWithTime(sprintf("Updates set [%s] for <%s>: %d pcs",
                            $client_updates_key, $user_id, count($client_updates)));
                    } catch (JsonException $e) {
                        $this->infoWithTime($e->getMessage());
                    }
                }
            }
        } else {
            $this->infoWithTime('No active users');
        }

        $this->clearUpdates();
        $this->infoWithTime("Updates cleared for $this->keyspace");

        return 0;
    }
}
