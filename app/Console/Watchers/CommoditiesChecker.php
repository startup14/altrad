<?php

namespace App\Console\Watchers;


use App\Http\Controllers\WebsocketController;

final class CommoditiesChecker extends BaseChecker implements ChangeChecker
{

    public function check(WebsocketController $controller, array $prev, array $banned_quotes = []): array
    {
        return $this->updateKeyspace('commodities', $prev, 'fallback_commodities', $banned_quotes);
    }

    public function justUpdates(WebsocketController $controller, array $prev): array
    {
        return $this->updateKeyspace('commodities', $prev);
    }
}
