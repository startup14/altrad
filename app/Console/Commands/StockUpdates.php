<?php

namespace App\Console\Commands;


class StockUpdates extends AbstractUpdates
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:updates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Spam stock updates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->keyspace = REDIS_STREAM_STOCKS;
        $this->fun_fallback = 'fallback_stocks';
        parent::__construct();
    }

    protected function clientUpdatesFilter(int $user_id): array
    {
        return [];
    }

    protected function clientUpdates(int $user_id, array $cache): array
    {
        $filter = $this->clientUpdatesFilter($user_id);
        return fallback_stocks($cache, $filter);
    }

    protected function needPrecache(): bool
    {
        return true;
    }

    protected function updatesInCache(): array
    {
        $updated_keys = redis_sget($this->update_key);
        return redis_hgetall_for_keys($updated_keys);
    }
}
