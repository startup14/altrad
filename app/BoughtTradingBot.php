<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoughtTradingBot extends Model
{
    protected $fillable = [
        'bot_id',
        'user_id',
        'active',
        'active_till'
    ];
}
