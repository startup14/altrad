<?php

namespace App\Console\DataProviders;

use App\Orders;
use App\OrdersHistory;
use App\Positions;
use App\User;
use Carbon\Carbon;
use Exception;

final class UserHop extends BaseUserDataProvider implements IUserDataProvider
{
    public function fetch(int $user_id): array
    {

        $aged = $this->aged();

        $updates = [];

        try {

            $user = User::select('id')
                ->whereId($user_id)
                ->first();
            if (!$user) {
                return ['warn' => 'Oops... User not found'];
            }

            $history_updates = OrdersHistory::whereUserId($user_id)
                ->where('created_at', '>', $aged)
                ->get();
            $positions_updates = Positions::whereUserId($user_id)
                ->where('created_at', '>', $aged)
                ->get();

            if ($history_updates) {
                $updates['history'] = OrdersHistory::where('user_id', $user->id)->orderBy('created_at')->get();
                $updates['positions'] = Positions::where('user_id', $user->id)->orderBy('created_at')->get();
                $updates['orders'] = Orders::where('user_id', $user->id)->orderBy('created_at')->get();
                return $updates;
            }

            if ($positions_updates) {
                $updates['positions'] = Positions::where('user_id', $user->id)->orderBy('created_at')->get();
                $updates['orders'] = Orders::where('user_id', $user->id)->orderBy('created_at')->get();
            } else {
                $positions_updates = Positions::where('user_id', $user->id)
                    ->where('updated_at', Carbon::now()->subSeconds()->setTimezone('UTC'))->get();
                if ($positions_updates) {
                    $updates['positions'] = Positions::where('user_id', $user->id)->orderBy('created_at')->get();
                    $updates['orders'] = Orders::where('user_id', $user->id)->orderBy('created_at')->get();
                }
            }
            return $updates;

        } catch (Exception $e) {
            $updates['error'] = $e->getMessage();
        }

        return $updates;
    }
}
