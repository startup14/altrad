<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientBannedActive extends Model
{
    public $timestamps = false;
    protected $fillable = ['user_id', 'pair'];
}
