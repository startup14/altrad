<?php

namespace App\Console\Tasks;

use App\HistoricalData;
use Exception;

class UpdateChart extends BaseTask  implements IBaseTask
{
    public function finish($controller): array
    {
        $response = 200;

        foreach ($controller->charts as $chart) {

            $chart = $controller->charts[$chart]['conn'];
            $interval = $controller->charts[$chart]['interval'] ?? '1d';
            $symbol = $controller->charts[$chart]['symbol'] ?? 'AMZN';
            $data = [];

            $check = HistoricalData::where('symbol', $symbol)
                ->whereTimePeriod($interval)
                ->latest('updated_at')
                ->first();

            if ($check) {
                $data = [
                    'kline' => [
                        'openTime' => (int)$check->open_time,
                        'closeTime' => (int)$check->close_time,
                        'symbol' => (string)$check->symbol,
                        'interval' => (string)$check->time_period,
                        'open' => (string)$check->open,
                        'close' => (string)$check->close,
                        'high' => (string)$check->high,
                        'low' => (string)$check->low,
                        'volume' => (string)$check->volume
                    ]
                ];
            }

            try {
                $chart->send(json_encode($data, JSON_THROW_ON_ERROR));
                $this->timeLine("Chart Updated for <$symbol>");
            } catch (Exception $e) {
                $this->notifyAllLogs($e->getMessage());
                $response = 500;
            }
        }

        return compact('response');

    }
}
