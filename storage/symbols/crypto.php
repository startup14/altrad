<?php

return [
    'BTC' => [
        'name' => 'Bitcoin',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100',
                'active' => 0,
            ],
        ]
    ],
    'BCH' => [
        'name' => 'Bitcoin Cash',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
        ]
    ],
    'BNB' => [
        'name' => 'Binance Coin',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100',
                'active' => 0,
            ],
        ]
    ],
    'ETH' => [
        'name' => 'Ethereum',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
        ]
    ],
    'LTC' => [
        'name' => 'Litecoin',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'XRP' => [
        'name' => 'Ripple',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'KSM' => [
        'name' => 'Kusama',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100',
                'active' => 0,
            ],
        ]
    ],
    'KNC' => [
        'name' => 'Kyber Network',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.1',
                'pricescale' => '1000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'REP' => [
        'name' => 'Augur',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.1',
                'pricescale' => '1000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'XTZ' => [
        'name' => 'Tezos',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000',
                'active' => 0,
            ],
        ]
    ],
    'XLM' => [
        'name' => 'Stellar',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'ZRX' => [
        'name' => '0rx',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'OMG' => [
        'name' => 'OMG Network',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'LINK' => [
        'name' => 'Chainlink',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.1',
                'pricescale' => '1000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'MKR' => [
        'name' => 'Maker',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
        ]
    ],
    'EOS' => [
        'name' => 'EOS',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'AION' => [
        'name' => 'AION',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'ADA' => [
        'name' => 'Cardano',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'TRX' => [
        'name' => 'TRON',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'XMR' => [
        'name' => 'Monero',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'NEO' => [
        'name' => 'NEO',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'ATOM' => [
        'name' => 'Cosmos',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '1000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000',
                'active' => 0,
            ],
        ]
    ],
    'ANT' => [
        'name' => 'Aragon',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000',
                'active' => 0,
            ],
        ]
    ],
    'ARDR' => [
        'name' => 'Ardor',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
        ]
    ],
    'BEAM' => [
        'name' => 'Beam',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000',
                'active' => 0,
            ],
        ]
    ],
    'BLZ' => [
        'name' => 'Bluzelle',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'BNT' => [
        'name' => 'Bancor',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'BTS' => [
        'name' => 'BitShares',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
        ]
    ],
    'BTT' => [
        'name' => 'BitTorrent Token',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000',
                'active' => 0,
            ],
        ]
    ],
    'CVC' => [
        'name' => 'Civic',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'DATA' => [
        'name' => 'Streamr DATAcoin',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '',
                'pricescale' => '',
                'active' => 0,
            ],
        ]
    ],
    'DCR' => [
        'name' => 'Decred',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.1',
                'pricescale' => '1000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
        ]
    ],
    'DENT' => [
        'name' => 'Dent',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000',
                'active' => 0,
            ],
        ]
    ],
    'DGB' => [
        'name' => 'DigiByte',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
        ]
    ],
    'DOCK' => [
        'name' => 'Dock',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
        ]
    ],
    'DOGE' => [
        'name' => 'Dogecoin',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000',
                'active' => 0,
            ],
        ]
    ],
    'ENJ' => [
        'name' => 'Enjin Coin',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
        ]
    ],
    'FUN' => [
        'name' => 'FunFair',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
        ]
    ],
    'GTO' => [
        'name' => 'Gifto',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ],
        ]
    ],
    'GXS' => [
        'name' => 'GXChain',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
        ]
    ],
    'ICX' => [
        'name' => 'Icon',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
        ]
    ],
    'IOST' => [
        'name' => 'IOST',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
        ]
    ],
    'IOTX' => [
        'name' => 'IoTeX',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '0.000001',
                'pricescale' => '100000000',
                'active' => 0,
            ],
        ]
    ],
    'ETC' => [
        'name' => 'Ethereum Classic',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.01',
                'pricescale' => '10000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000',
                'active' => 0,
            ],
            'ETH' => [
                'minmov' => '0.0001',
                'pricescale' => '1000000',
                'active' => 0,
            ],
        ]
    ],
    'BTG' => [
        'name' => 'Bitcoin Gold',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.1',
                'pricescale' => '1000',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000',
                'active' => 0,
            ]
        ]
    ],
    'DASH' => [
        'name' => 'Dash',
        'quotes' => [
            'USDT' => [
                'minmov' => '1',
                'pricescale' => '100',
                'active' => 0,
            ],
            'BTC' => [
                'minmov' => '0.001',
                'pricescale' => '100000',
                'active' => 0,
            ]
        ]
    ],
    'SHIB' => [
        'name' => 'SHIBA INU',
        'quotes' => [
            'USDT' => [
                'minmov' => '0.00001',
                'pricescale' => '10000000',
                'active' => 0,
            ],
        ]
    ]
];
