<?php

namespace App\Console\Actions;

use App\ClientBannedActive;

class GetClientBannedQuotes implements ParentAction
{

    public function commit(...$args): array
    {
        $user_id = $args[0];
        return ClientBannedActive::whereUserId($user_id)->pluck('pair')->toArray();
    }
}
