<?php

namespace App\Console;

use Spatie\ShortSchedule\ShortSchedule;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    public const USER_DATA_REFRESH_SEC = 7;

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CryptoUpdates::class,
        Commands\StockUpdates::class,
        Commands\CommoUpdates::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
    }

    protected function shortSchedule(ShortSchedule $shortSchedule): void
    {
        $shortSchedule->command('crypto:updates')->everySecond()->withoutOverlapping();
        $shortSchedule->command('stock:updates')->everySecond()->withoutOverlapping();
        $shortSchedule->command('commo:updates')->everySecond()->withoutOverlapping();

        $shortSchedule->command('user:settings')->everySeconds(self::USER_DATA_REFRESH_SEC)->withoutOverlapping();
        $shortSchedule->command('user:hop')->everySeconds(self::USER_DATA_REFRESH_SEC)->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');
        require base_path('routes/console.php');
    }
}
