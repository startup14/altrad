<?php

namespace App\Console\Watchers;


use App\Http\Controllers\WebsocketController;

final class StocksChecker extends BaseChecker implements ChangeChecker
{

    public function check(WebsocketController $controller, array $prev, array $banned_quotes = []): array
    {
        return $this->updateKeyspace('stocks', $prev, 'fallback_stocks', $banned_quotes);
    }

    public function justUpdates(WebsocketController $controller, array $prev): array
    {
        return $this->updateKeyspace('stocks', $prev);
    }
}
