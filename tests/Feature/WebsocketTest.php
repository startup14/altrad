<?php

// {"chart":"chart"}
// {"symbol":"BTCUSDT","interval":"1d"}
//
// {"id":60772,"broker":false}
// {"type":"crypto","pair":"USDT","currency":"BTC"}
//
// {"type":"stock","currency":"AAPL"}
// {"type":"commodities","currency":"XAU"}

use App\Console\Commands\WebsocketOpen;
use React\Socket\ConnectionInterface;

uses(Tests\TestCase::class);

test('Binance WS Crypto', function () {

    $data = true;

    $ws_client = new React\Socket\Connector();
    $ws_client->connect(WebsocketOpen::WS_DEFAULT_ADDR)->then(
        function (ConnectionInterface $conn) use (&$data) {
            $conn->write('{"id":60772,"broker":false}');

            $conn->on('data', function ($chunk) use (&$data) {
                $data = $chunk;
            });
        }
    );

    $redis_key = redis_create_crypto_key('BTC:USDT');
    $new = redis_hgetall($redis_key);
    $new['p'] += 0.01;
    redis_hmset($redis_key, $new);

    $this->assertTrue($data);
});
