<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricalData extends Model
{
    protected $table = 'historical_data';

    protected $fillable = [
        'open_time',
        'close_time',
        'close',
        'high',
        'low',
        'open',
        'time_period',
        'symbol',
        'volume'
    ];
}
