<?php

namespace App\Console\DataProviders;

use Carbon\Carbon;

abstract class BaseUserDataProvider
{
    public const USER_AGED_SEC = 7;

    protected function aged(): Carbon
    {
        return Carbon::now()->subSeconds(self::USER_AGED_SEC)->setTimezone('UTC');
    }
}
