<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradingBotReview extends Model
{
    protected $fillable = [
        'bot_id',
        'author',
        'rating',
        'text'
    ];
}
