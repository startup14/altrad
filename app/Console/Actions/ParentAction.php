<?php

namespace App\Console\Actions;

interface ParentAction
{
    public function commit(...$args): array;
}
