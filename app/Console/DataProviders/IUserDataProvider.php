<?php

namespace App\Console\DataProviders;

interface IUserDataProvider
{
    public function fetch(int $user_id): array;
}
