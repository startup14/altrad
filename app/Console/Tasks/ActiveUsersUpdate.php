<?php

namespace App\Console\Tasks;

class ActiveUsersUpdate extends BaseTask implements IBaseTask
{

    public function finish($controller): array
    {
        return $controller->activeUsers();
    }
}
