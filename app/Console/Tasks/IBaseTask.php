<?php

namespace App\Console\Tasks;

interface IBaseTask
{
    public function finish($controller): array;
}
