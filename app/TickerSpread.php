<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TickerSpread extends Model
{
    protected $fillable = [
        'spread',
        'ticker'
    ];
}
