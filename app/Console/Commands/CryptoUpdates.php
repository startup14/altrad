<?php

namespace App\Console\Commands;

use App\Console\Actions\GetClientBannedQuotes;
use App\Console\Traits\SmartOutput;

class CryptoUpdates extends AbstractUpdates
{
    use SmartOutput;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crypto:updates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Spam crypto updates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->keyspace = REDIS_STREAM_CRYPTO;
        parent::__construct();
    }

    protected function clientUpdatesFilter(int $user_id): array
    {
        // todo: keep banned in cache too.
        return (new GetClientBannedQuotes())->commit($user_id);
    }

    protected function clientUpdates(int $user_id, array $cache): array
    {
        $filter = $this->clientUpdatesFilter($user_id);
        return fallback_crypto($cache, $filter);
    }

    protected function needPrecache(): bool
    {
        return true;
    }

    protected function updatesInCache(): array
    {
        $updated_keys = redis_sget($this->update_key);
        return redis_hgetall_for_keys($updated_keys);
    }
}
