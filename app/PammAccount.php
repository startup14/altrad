<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PammAccount extends Model
{
    protected $fillable = [
        'owner_id',
        'name',
        'description',
        'balance',
        'profit'
    ];
}
