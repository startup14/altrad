<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_history', function (Blueprint $table) {
            $table->id();
            $table->string('status')->default('open');
            $table->string('class');
            $table->double('entry_price')->nullable();
            $table->string('type');
            $table->double('amount');
            $table->double('limit');
            $table->double('value');
            $table->string('pair');
            $table->string('currency');
            $table->double('liq');
            $table->double('lv');
            $table->integer('user_id');
            $table->double('leverage');
            $table->double('take_profit')->nullable();
            $table->double('stop_loss')->nullable();
            $table->double('close');
            $table->timestamp('time_of_open');
            $table->string('type_of_market');
            $table->string('unit');
            $table->double('balance_before_open');
            $table->double('balance_after_open');
            $table->double('balance_before_close');
            $table->double('balance_after_close');
            $table->double('swop')->default(0);
            $table->double('crypto_amount');
            $table->double('conversion')->default('1');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_history');
    }
}
