<?php

namespace App\Console\Watchers;

use App\Http\Controllers\WebsocketController;
use App\User;
use Carbon\Carbon;

class UserSpecificsChecker extends BaseChecker implements ChangeChecker
{
    public function check($controller, array $prev = []): array
    {

        $updates = [];

        $last_updated = Carbon::now()->subSeconds(WebsocketController::USER_FRESH_SEC)->setTimezone('UTC');

        $user = User::select(
            'id',
            'crypto_fee',
            'stock_fee',
            'commodities_fee',
            'positions_fee',
            'balance',
            'account_status',
            'is_active',
            'min_deal',
            'min_deal_stock',
            'min_deal_commodities'
        )
            ->whereId($controller['user_id'])
            ->where('updated_at', '>=', $last_updated)
            ->orderByDesc('updated_at')
            ->first();

        if ($user) {
            $fees = [
                'crypto_fee' => $user->crypto_fee,
                'stock_fee' => $user->stock_fee,
                'commodities_fee' => $user->commodities_fee,
                'positions_fee' => $user->positions_fee
            ];

            $updates = [
                'fees' => $fees,
                'balance' => $user->balance,
                'account_status' => $user->account_status,
                'is_active' => $user->is_active,
                'min_deal' => $user->min_deal,
                'min_deal_stock' => $user->min_deal_stock,
                'min_deal_commodities' => $user->min_deal_commodities
            ];
        }

        return $updates;
    }

    public function justUpdates(WebsocketController $controller, array $prev): array
    {
        return $this->check($controller, $prev);
    }
}
