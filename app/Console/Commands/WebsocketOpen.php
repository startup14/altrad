<?php

namespace App\Console\Commands;

use App\Console\Actions\GetClientBannedQuotes;
use App\Console\Traits\SmartOutput;
use App\Console\Traits\SystemStatsTrait;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Console\Tasks\{ActiveUsersUpdate,
    BaseTask,
    IBaseTask,
    UpdateChart,
    ConnectionsRefresh,
    LogoutSender,
    CloseConnectionSender};

use App\Console\Watchers\{
    ChangeChecker,
    CommoditiesChecker,
    CryptoChecker,
    ForceUpdateTask,
    OnlineUsersChecker,
    OrderHistoryChecker,
    OrdersChecker,
    StocksChecker,
    UserSpecificsChecker};

use App\Http\Controllers\WebsocketController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

use React\EventLoop\Loop;
use React\EventLoop\LoopInterface;
use React\Socket\SecureServer;
use React\Socket\SocketServer;


final class WebsocketOpen extends Command
{
    use SystemStatsTrait;
    use SmartOutput;

    public const WS_CLIENT_LOOP_CHUNK = 50;

    public const WS_TCP_OPTIONS = [
        'backlog' => 100000,
        'so_reuseport' => true,
        'so_broadcast' => false,
    ];

    public array $WS_LOOP_CONTEXT;
    public const WS_DEFAULT_ADDR = '0.0.0.0:2096';

    protected $signature = 'websocket:open';
    protected $description = 'Opens websocket server to get data from';

    private array $KEYSPACES;

    // designates working in security mode on production
    private bool $WSS_MODE = false;

    // Cache
    protected array $crypto = [];
    protected array $stocks = [];
    protected array $commodities = [];

    // Watchers
    private ChangeChecker $onlineUsersWatcher;
    private ChangeChecker $stocksWatcher;
    private ChangeChecker $commoditiesWatcher;
    private ChangeChecker $cryptoWatcher;
    private ChangeChecker $userSpecificsWatcher;
    private ChangeChecker $orderHistoryWatcher;
    private ChangeChecker $ordersWatcher;
    private IBaseTask $forceUpdateWatcher;

    // Tasks
    private IBaseTask $closeConnectionSender;
    private BaseTask $logoutSender;
    private IBaseTask $connectionsRefresh;
    private IBaseTask $updateChartTask;

    private IoServer $server;

    private function isLocalDeploy(): bool
    {
        return App::environment() === 'local';
    }

    private function isDevDeploy(): bool
    {
        return  in_array(App::environment(), ['dev', 'develop']);
    }

    private function isLocalOrDevDeploy(): bool
    {
        return $this->isLocalDeploy() || $this->isDevDeploy();
    }

    private function createSocketServer(LoopInterface $loop)
    {
        $server = new SocketServer(self::WS_DEFAULT_ADDR, self::WS_TCP_OPTIONS, $loop);
        if (! $this->isLocalDeploy()) {
            $server = new SecureServer($server, $loop, $this->WS_LOOP_CONTEXT);
            $this->WSS_MODE = true;
        }
        return $server;
    }

    private function timeLine(string $line): void
    {
        $this->line($this->smartLine($line));
        $this->redisLine($line);
    }

    private function timeLineColor(string $color_line, string $color, int $count): void
    {
        $this->timeLine("$color:$color_line diffs: $count");
    }

    private function clarifyException(Exception $e): string
    {
        return sprintf('Error: %s - [%s]', get_class($e), $e->getMessage());
    }

    private function logMemoryUsage(): void
    {
        $this->timeLine($this->memoryStatLine());
    }

    private function loadQuotes(): int
    {
        $response = 200;

        try {
            $this->crypto = redis_hgetall_for_keyspace(REDIS_STREAM_CRYPTO);
            $this->stocks = redis_hgetall_for_keyspace(REDIS_STREAM_STOCKS);
            $this->commodities = redis_hgetall_for_keyspace(REDIS_STREAM_COMMO);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            $this->timeLine($this->clarifyException($e));
            $response = 503;
        }

        return $response;
    }

    public function __construct()
    {
        parent::__construct();

        $this->WS_LOOP_CONTEXT = [
            'local_cert' => env('LOCAL_CERT'),
            'local_pk' => env('LOCAL_PK'),
            'allow_self_signed' => true,
            'verify_peer' => false
        ];

        $this->KEYSPACES = [
            REDIS_STREAM_CRYPTO => 'cryptoWatcher',
            REDIS_STREAM_STOCKS => 'stocksWatcher',
            REDIS_STREAM_COMMO => 'commoditiesWatcher',
        ];

        $this->onlineUsersWatcher = new OnlineUsersChecker();
        $this->stocksWatcher = new StocksChecker();
        $this->commoditiesWatcher = new CommoditiesChecker();
        $this->cryptoWatcher = new CryptoChecker();
        $this->userSpecificsWatcher = new UserSpecificsChecker();
        $this->orderHistoryWatcher = new OrderHistoryChecker();
        $this->ordersWatcher = new OrdersChecker();
        $this->forceUpdateWatcher = new ForceUpdateTask();

        $this->logoutSender = new LogoutSender();
        $this->closeConnectionSender = new CloseConnectionSender();
        $this->connectionsRefresh = new ConnectionsRefresh();
        $this->updateChartTask = new UpdateChart();

        $this->mem_usage = memory_get_usage(true);
    }

    private function initWSLoop($controller): LoopInterface
    {
        $loop = Loop::get();

        $webSock = $this->createSocketServer($loop);
        $wsServer = new WsServer($controller);
        $this->server = new IoServer(
            new HttpServer(
                $wsServer
            ),
            $webSock,
            $loop
        );

        $wsServer->enableKeepAlive($this->server->loop);

        $this->timeLine('WS Server started on ' . self::WS_DEFAULT_ADDR);
        if ($this->WSS_MODE) {
            $this->timeLine('In WSS mode.');
        }

        return $loop;
    }

    private function clientCheckerUpdates($client, string $keyspace, ChangeChecker $watcher): array
    {
        $start = hrtime(true);
        $updates = $watcher->check($client);
        $last_for_secs = $this->ns2sec(hrtime(true) - $start);

        $this->timeLine(sprintf("LOOP: Check $keyspace changes: [%F]", $last_for_secs));
        $this->logMemoryUsage();

        return $updates;
    }

    private function nextTaskUpdates($client, string $task_name, IBaseTask $task): array
    {
        $start = hrtime(true);
        $updates = $task->finish($client);
        $last_for_secs = $this->ns2sec(hrtime(true) - $start);

        $this->timeLine(sprintf("LOOP: Execute $task_name changes: [%F]", $last_for_secs));
        $this->logMemoryUsage();

        return $updates;
    }

    private function attachWatchers(LoopInterface $loop, $controller): void
    {
        // Check online user
        $loop->addPeriodicTimer(10, function () use ($controller) {
            $onlines = $this->onlineUsersWatcher->check($controller);
            $this->timeLine(sprintf("Updating TOTAL CLIENT online: [%d]", count($onlines)));
            $this->logMemoryUsage();
        });

        // Update Chart
        $loop->addPeriodicTimer(3, function () use ($controller) {
            $this->timeLine('Update KLINEs...');
            $this->updateChartTask->finish($controller);
            $this->logMemoryUsage();
        });

        // main loop
        $loop->addPeriodicTimer(1, function () use ($controller) {

            $big_loop_start_time = hrtime(true);
            $this->timeLine("BIG LOOP: Started.");

            // lock, stock and two smoking barrels...
            $assets_updates = [];
            foreach ($this->KEYSPACES as $keyspace => $watcher) {
                $loop_start_time = hrtime(true);
                $KEYSPACE_UPPER = strtoupper($keyspace);
                ['current' => $current, 'updates' => $updates] = $this->$watcher->justUpdates($controller, $this->$keyspace);
                $this->$keyspace = $current;
                $assets_updates[$keyspace] = $updates;
                $last_for_secs = $this->ns2sec(hrtime(true) - $loop_start_time);
                $this->timeLine(sprintf("LOOP: Checked updates for [%s]: [%F]", $KEYSPACE_UPPER, $last_for_secs));
                $this->logMemoryUsage();
            }

            // client updates
            foreach ($controller->clients as $client) {
                $client_loop_start_time = hrtime(true);
                $this->timeLine('CLIENT LOOP Started');

                $this->timeLine(sprintf("LOOP: Check updates for client [%s]", $client->remoteAddress));
                $client_updates = [];
                $client = $controller->clients[$client];

                // filter by banned
                $banned_assets = (new GetClientBannedQuotes())->commit($client['user_id']);
                foreach (array_keys($this->KEYSPACES) as $keyspace) {
                    if (!$assets_updates[$keyspace]) {
                        continue;
                    }
                    $keyspace_start = hrtime(true);
                    $front_transformer = "fallback_$keyspace";
                    $client_updates[$keyspace] = $front_transformer($assets_updates[$keyspace], $banned_assets);
                    $keyspace_secs = $this->ns2sec(hrtime(true) - $keyspace_start);
                    $this->timeLine(sprintf("LOOP: Convert %s updates: [%F]", strtoupper($keyspace), $keyspace_secs));
                }

                // + user_details
                $client_updates += $this->clientCheckerUpdates($client, 'user_details', $this->userSpecificsWatcher);
                // + order, positions, history
                $client_updates += $this->clientCheckerUpdates($client, 'orders, history, positions', $this->orderHistoryWatcher);
                // + market, depth
                $client_updates += $this->clientCheckerUpdates($client, 'market, depth', $this->ordersWatcher);

                // + Refresh connections
                $client_updates += $this->nextTaskUpdates($client, 'Refresh active connections', $this->connectionsRefresh);
                // + Close non-active connections
                $client_updates += $this->nextTaskUpdates($client, 'Close non-active connections', $this->closeConnectionSender);
                // + Logout
                $client_updates += $this->nextTaskUpdates($client, 'Logout by inactivity timeout', $this->logoutSender);
                // + Check force updates
                $client_updates += $this->nextTaskUpdates($client, 'Check force-updates', $this->forceUpdateWatcher);

                if (empty($client_updates)) {
                    $this->timeLine("Client Updates are empty. Hope be lucky next time...");
                    continue;
                }

                $conn = $client['conn'];

                $this->timeLine("LOOP: Send update to client $conn->remoteAddress...");
                $task_start_time = hrtime(true);
                try {
                    $conn->send(json_encode($client_updates, JSON_THROW_ON_ERROR));
                    $task_secs = $this->ns2sec(hrtime(true) - $task_start_time);
                    $this->timeLine(sprintf("LOOP: Send Done: [%F]", $task_secs));
                } catch (Exception $e) {
                    $this->timeLine('Error: ' . $e->getMessage());
                } finally {
                    $this->timeLine(sprintf('CLIENT LOOP Ended: [%F]', $this->ns2sec(hrtime(true) - $client_loop_start_time)));
                }
            }

            $this->timeLine(sprintf("BIG LOOP: Ended: [%F]", $this->ns2sec(hrtime(true) - $big_loop_start_time)));
        });
    }

    private function ns2sec(int $ns): float
    {
        return round($ns/1e9, 8);
    }

    protected array $escaped = [
        REDIS_KEYSPACE_USER_SETTINGS,
        REDIS_KEYSPACE_USER_HOP,
    ];

    private function injectKeyspace(string $json, string $curr_key): ?string
    {
        $keyspace = array_last(explode(REDIS_KEY_SEPARATOR, $curr_key));
        if (in_array($keyspace, $this->escaped, true)) {
            return $json;
        }

        $new_json = sprintf('{"%s":%s}', $keyspace, $json);

        try {
            $test_arr = json_decode($new_json, true, 512, JSON_THROW_ON_ERROR);
            return array_key_exists($keyspace, $test_arr) ? $new_json : null;
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }

        return null;
    }

    private function attachTasks(LoopInterface $loop, $controller): void
    {
        $loop->addPeriodicTimer(1, function() use ($controller) {

            // update active users in Redis
            $users = (new ActiveUsersUpdate())->finish($controller);
            redis_sadd(REDIS_ACTIVE_USERS, $users);
            $this->infoWithTime(sprintf("Update Active Users: %d", count($users)));

            // spam users updates
            foreach ($controller->clients as $client) {
                $client = $controller->clients[$client];
                $user_id = $client['user_id'];
                $user_data_key = redis_create_user_key($user_id);
                $user_updates_keys = redis_keys_by_raw_pattern("$user_data_key:*");
                foreach ($user_updates_keys as $curr_updates_key) {
                    // how about updates?
                    $updates_json = redis_get($curr_updates_key);
                    $updates_json = $this->injectKeyspace($updates_json, $curr_updates_key);
                    if ($updates_json) {
                        // send updates
                        $client['conn']->send($updates_json);
                        $this->infoWithTime("Updates sent to [$curr_updates_key]");
                    }
                    // clear key to refresh
                    redis_unlink($curr_updates_key);
                    $this->infoWithTime("Key unlinked: [$curr_updates_key]");
                }
            }
        });
    }

    public function handle(): void
    {
        try {

//            if ($this->loadQuotes() !== 200) {
//                $this->infoWithTime('Can`t load quotes...Exiting.');
//                return;
//            }

            $controller = new WebsocketController();

            $loop = $this->initWSLoop($controller);
            $this->infoWithTime('LOOP started');

//            $this->attachWatchers($loop, $controller);
            $this->attachTasks($loop, $controller);
            $this->infoWithTime('Tasks Attached');

            $this->server->run();
        } catch (Exception $e) {
            $error = $this->clarifyException($e);
            Log::error($error);
            $this->timeLine($error);
        }
    }

    private function redisLine(string $line): void
    {
        redis_xadd('ws:log', compact('line'));
    }

}
