<?php

namespace App\Console\Commands;


final class UserSettings extends AbstractUpdates
{
    protected $signature = 'user:settings {user_id?}';

    protected $description = 'Updates user details (fees, history, etc...) in Redis';

    public function __construct()
    {
        $this->keyspace = REDIS_KEYSPACE_USER_SETTINGS;
        parent::__construct();
    }

    protected function clientUpdatesFilter(int $user_id): array
    {
        return [];
    }

    protected function clientUpdates(int $user_id, array $cache = []): array
    {
        return (new \App\Console\DataProviders\UserSettings())->fetch($user_id);
    }

    protected function needPrecache(): bool
    {
        return false;
    }

    protected function updatesInCache(): array
    {
        return [];
    }
}
