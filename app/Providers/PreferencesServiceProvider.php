<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\PreferencesService;
use App\Contracts\PreferencesInterface;

class PreferencesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PreferencesInterface::class, PreferencesService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
