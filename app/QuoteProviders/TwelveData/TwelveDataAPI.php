<?php

namespace App\QuoteProviders\TwelveData;

use App\QuoteProviders\BaseAPI;
use App\Contracts\StocksProviderInterface;

class TwelveDataAPI extends BaseAPI implements StocksProviderInterface
{
    /**
     * @var string
     */
    protected string $endpoint = "https://api.twelvedata.com";

    /**
     * @var array
     */
    protected array $queryParams = [];


    public function __construct()
    {
        //parent::__construct();

        $this->queryParams = [
            'apikey' => env('TWELVE_API_KEY'),
        ];
    }

    public function getTickers(array $symbols = []): array
    {
        $tickers = [];

        $response = $this->send('quote', [
            'symbol' => implode(',', $symbols),
        ]);

        if (isset($response)) {
            $response = $response->json();

            foreach ($response as $symbol => $ticker) {
                if (in_array($symbol, $symbols)) {
                    $tickers[$symbol] = $this->tickerDecoration($ticker);
                }
            }
        }

        return $tickers;
    }

    private function tickerDecoration(array $ticker = []): array
    {
        return [
            'symbol' => $ticker['symbol'],
            'open' => isset($ticker['open'])
                ? round((DOUBLE)$ticker['open'], 2)
                : null,
            'close' => isset($ticker['close'])
                ? round((DOUBLE)$ticker['close'], 2)
                : null,
            'prev_close' => isset($ticker['previous_close'])
                ? round((DOUBLE)$ticker['previous_close'], 2)
                : null,
            'high' => isset($ticker['high'])
                ? round((DOUBLE)$ticker['high'], 2)
                : null,
            'low' => isset($ticker['low'])
                ? round((DOUBLE)$ticker['low'], 2)
                : null,
            'vol' => $ticker['volume'] ?? null,
        ];
    }
}
