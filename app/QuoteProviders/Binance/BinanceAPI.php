<?php

namespace App\QuoteProviders\Binance;

use App\QuoteProviders\BaseAPI;
use App\Contracts\CryptoProviderInterface;

class BinanceAPI extends BaseAPI implements CryptoProviderInterface
{
    /**
     * @var string
     */
    protected string $endpoint = "https://api.binance.com/api/v3";

    /**
     * @var array
     */
    protected array $queryParams = [];

    public function getKline(string $symbol): ?array
    {
        $response = $this->send('klines', [
            'symbol' => $symbol,
        ]);

        $tickers = [];
        if (isset($response)) {
            $response = $response->json();
            foreach ($response as $ticker) {
                $tickers[] = $this->klineDecoration($ticker, $symbol);
            }
        }
        return $tickers;
    }

    public function getTickers(array $symbols = []): array
    {
        $tickers = [];

        foreach ($symbols as $baseSymbol => $base) {
            foreach ($base as $quoteSymbol) {
                $tickers[] = $this->getTicker($baseSymbol, $quoteSymbol);
            }
        }

        return $tickers;
    }

    public function getTicker(string $base, string $quote): ?array
    {
        $response = $this->send('ticker/24hr', [
            'symbol' => "{$base}{$quote}",
        ]);

        return isset($response)
            ? $this->tickerDecoration($response->json(), $base, $quote)
            : null;
    }

    private function tickerDecoration(array $ticker, string $base, string $quote): array
    {
        return [
            'symbol' => "{$base}{$quote}",
            'base' => $base,
            'quote' => $quote,
            'open_time' => $ticker['openTime'],
            'close_time' => $ticker['closeTime'],
            'open' => (DOUBLE)$ticker['openPrice'],
            'high' => (DOUBLE)$ticker['highPrice'],
            'low' => (DOUBLE)$ticker['lowPrice'],
            'close' => (DOUBLE)$ticker['lastPrice'],
            'vol' => (DOUBLE)$ticker['volume'],
        ];
    }
    private function klineDecoration(array $ticker, string $symbol): array
    {
        return [
            'symbol' => $symbol,
            'open_time' => $ticker[0],
            'close_time' => $ticker[6],
            'open' => (DOUBLE)$ticker[1],
            'high' => (DOUBLE)$ticker[2],
            'low' => (DOUBLE)$ticker[3],
            'close' => (DOUBLE)$ticker[4],
            'vol' => (DOUBLE)$ticker[5],
        ];
    }
}
