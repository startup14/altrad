<?php

namespace App\Console\Watchers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

abstract class BaseChecker
{
    public function isMarketOpen(): bool
    {
        return (Cache::get('market'))['open'] ?? false;
    }

    private function isLocalOrDevDeploy(): bool
    {
        return in_array(App::environment(), ['local', 'dev', 'develop']);
    }

    protected function timeLine(string $line): void
    {
        if ($this->isLocalOrDevDeploy()) {
            $output = sprintf('[%s] %s.%s', date('Y-m-d H:i:s'), $line, PHP_EOL);
            echo $output;
        }
    }

    protected function notifyAllLogs(string $message): void
    {
        Log::error($message);
    }

    protected function lookForChanges(string $keyspace, array $old): array
    {
        $updates = [];
        $current = redis_hgetall_for_keyspace($keyspace);

        foreach ($current as $redis_key => $new) {
            if (!isset($old[$redis_key])) {
                $updates[$redis_key] = $new;
                continue;
            }
            $difference = array_diff($new, $old[$redis_key]);
            if (!empty($difference)) {
                $updates[$redis_key] = $old[$redis_key] = $new;
            }
        }

        return compact('updates', 'current');
    }

    protected function convertUpdatesForClients(
        string $keyspace, array $diffs,
        string $legacy_format_cb = null, array $banned_quotes = []): array
    {
        $to_send = $legacy_format_cb ? $legacy_format_cb($diffs, $banned_quotes) : $diffs;
        ksort($to_send);
        $this->timeLine("$keyspace data converted for clients.");
        return $to_send;
    }

    protected function updateKeyspace(
        string $keyspace, array $prev,
        string $legacy_format_cb = null, array $banned_quotes = []): array
    {
        $changes = $this->lookForChanges($keyspace, $prev);
        ['updates' => $updates, 'current' => $current] = $changes;

        $to_sent = [];
        if ($legacy_format_cb && $updates) {
            $to_sent = $this->convertUpdatesForClients($keyspace, $updates, $legacy_format_cb, $banned_quotes);
        }

        return compact('updates', 'current', 'to_sent');
    }
}
