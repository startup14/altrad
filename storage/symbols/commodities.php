<?php


return [
    'XAU' => [
        'graph' => 'XAUUSD',
        'name' => 'Gold',
        'name_short' => 'Gold',
        'units' => 'OZ',
        'active' => 0,
    ],
    'XAG' => [
        'graph' => 'XAGUSD',
        'name' => 'Silver',
        'name_short' => 'Silver',
        'units' => 'OZ',
        'active' => 0,
    ],
    'XPT' => [
        'graph' =>'XPTUSD',
        'name' => 'Platinum',
        'name_short' => 'Platinum',
        'units' => 'OZ',
        'active' => 0,
    ],
    'XPD' => [
        'graph' => 'XPDUSD',
        'name' => 'Palladium',
        'name_short' => 'Palladium',
        'units' => 'OZ',
        'active' => 0,
    ]
];
