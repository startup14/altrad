<?php

namespace App\Console\Tasks;

use App\Http\Controllers\WebsocketController;

class ContactClient extends BaseTask
{
    public function say(string $message, WebsocketController $controller): int
    {
        $response = 200;

        $active_clients = $controller->clients;
        echo sprintf("Active clients: %d \n", count($active_clients));

        foreach ($active_clients as $client) {
            try {
                $client->send(
                    json_encode(compact('message'), JSON_THROW_ON_ERROR)
                );
            } catch (\Exception $e) {
                $this->notifyAllLogs($e->getMessage());
                $response = 500;
            }
        }

        return $response;
    }
}
