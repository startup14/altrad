<?php

namespace App\QuoteProviders;

use React\EventLoop;
use Ratchet\Client as Ratchet;
use Ratchet\RFC6455\Messaging;

use App\Logger;

abstract class BaseWebSocket
{

    protected $app;
    protected $loop;
    protected Ratchet\Connector $connector;

    /**
     * @var array
     */
    private array $subscribes;

    /**
     * @var
     */
    private  $onMessage;

    public function __construct(callable $onMessage, array $subscribes = [])
    {
        $this->onMessage = $onMessage;
        $this->subscribes = $subscribes;

        $this->connect();
    }

    private function connect(): void
    {
        $this->loop = EventLoop\Factory::create();
        $this->connector = new Ratchet\Connector($this->loop);
        $this->reconnect();
        $this->loop->run();
    }

    protected function reconnect(): void
    {
        ($this->connector)($this->endpoint)->then(
            function (Ratchet\WebSocket $connect) {
                $connect->on('message', function (Messaging\MessageInterface $message) {
                    try {
                        $msg = json_decode($message, true);
                        ($this->onMessage)($msg);
                    } catch(\Exception $exception) {
                        echo $exception->getMessage();
                    }
                });

                $connect->on('close', function ($code = null, $reason = null) {
                    $this->onClose($code, $reason);
                });

                $connect->on('ping', function (Messaging\FrameInterface $frame) use ($connect) {
                    $connect->send(new Messaging\Frame('', true, Messaging\Frame::OP_PING));
                });

                $flag = true;
                $connect->on('pong', function (Messaging\FrameInterface $frame) use ($connect, &$flag) {
                    $connect->send(new Messaging\Frame('', true, Messaging\Frame::OP_PONG));
                });

                foreach ($this->subscribes as $subscribe) {
                    $connect->send(
                        new Messaging\Frame(json_encode($subscribe), true, Messaging\Frame::OP_TEXT)
                    );
                }
            },
            function (\Exception $exception) {
                Logger::create([
                    'description' => "Could not connect: {$exception->getMessage()}"
                ]);
                $this->loop->stop();
            }
        );
    }

    private function onClose($code = null, $reason = null): void
    {
        $this->loop->addTimer(3, function () {
            $this->reconnect();
        });
    }
}
