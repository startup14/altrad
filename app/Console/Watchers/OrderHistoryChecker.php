<?php

namespace App\Console\Watchers;

use App\Http\Controllers\WebsocketController;
use App\Orders;
use App\OrdersHistory;
use App\Positions;
use App\User;
use Carbon\Carbon;
use Exception;

class OrderHistoryChecker extends BaseChecker implements ChangeChecker
{

    public function check($controller, array $prev = []): array
    {

        $updates = [];

        $user_id = $controller['user_id'];
        $history_freshened_at = Carbon::now()->subSeconds(WebsocketController::USER_FRESH_SEC)->setTimezone('UTC');

        try {

            $user = User::select('id')
                ->whereId($controller['user_id'])
                ->first();
            if (!$user) {
                return ['warn' => 'Oops... User not found'];
            }

            $history_updates = OrdersHistory::whereUserId($user_id)
                ->where('created_at', '>=', $history_freshened_at)
                ->get();
            $positions_updates = Positions::whereUserId($user_id)
                ->where('created_at', '>=', $history_freshened_at)
                ->get();
            $orders_updates = Orders::whereUserId($user_id)
                ->where('created_at', '>=', $history_freshened_at)
                ->get();

            if (! $history_updates->isEmpty()) {
                $updates['history'] = OrdersHistory::where('user_id', $user->id)->orderBy('created_at')->get();
                $updates['positions'] = Positions::where('user_id', $user->id)->orderBy('created_at')->get();
                $updates['orders'] = Orders::where('user_id', $user->id)->orderBy('created_at')->get();
            }

            if (! $positions_updates->isEmpty()) {
                $updates['positions'] = Positions::where('user_id', $user->id)->orderBy('created_at')->get();
                $updates['orders'] = Orders::where('user_id', $user->id)->orderBy('created_at')->get();
            } else {
                $positions_updates = Positions::where('user_id', $user->id)->where('updated_at', Carbon::now()->subSeconds(1)->setTimezone('UTC'))->get();
                if (! $positions_updates->isEmpty()) {
                    $updates['positions'] = Positions::where('user_id', $user->id)->orderBy('created_at')->get();
                    $updates['orders'] = Orders::where('user_id', $user->id)->orderBy('created_at')->get();
                }
            }

            if (!$orders_updates->isEmpty()) {
                $updates['orders'] = Orders::where('user_id', $user->id)->get();
            }

        } catch (Exception $e) {
            $updates['error'] = $e->getMessage();
            $this->timeLine('Error: ' . $e->getMessage());
        }

        return $updates;
    }

    public function justUpdates(WebsocketController $controller, array $prev): array
    {
        return $this->check($controller, $prev);
    }
}
