<?php

namespace App\Console\Traits;

use Illuminate\Support\Carbon;

trait SmartOutput
{
    // todo: add hrtime, get_mem_usage metrics
    public function smartLine(string $line): string
    {
        $signature = $this->signature ?? 'noname:process';
        return sprintf('%s -> [%s]: %s', Carbon::now(), $signature, $line);
    }

    public function smartLineCrlf(string $line): string
    {
        return $this->smartLine($line) . PHP_EOL;
    }

    public function infoWithTime(string $line): void
    {
        $this->info($this->smartLine($line));
    }
}
