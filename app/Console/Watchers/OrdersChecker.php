<?php

namespace App\Console\Watchers;

use App\Http\Controllers\WebsocketController;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Cache;

class OrdersChecker extends BaseChecker implements ChangeChecker
{

    public function check($controller, array $prev = []): array
    {
        // Redeploy

        $updates = [];

        try {
            $client = $controller['conn'];
            $type = $controller['type'];
            $currency = $controller['currency'];
            $pair = $controller['pair'];

            $user = User::select('id')
                ->whereId($controller['user_id'])
                ->first();

            $market = Cache::get('market');

            $updates = [
                'market' => $market,
            ];

            $ordersKey = "orders:$currency";
            $tradesKey = "trades:$currency";

            switch ($type) {
                // todo: look on dev for $pair var
                case 'crypto':
                    $ordersKey = "orders:$currency:$pair";
                    $tradesKey = "trades:$currency:$pair";
                    break;
                case 'stock':
                case 'commodities':
                    $ordersKey = "orders:$currency";
                    $tradesKey = "trades:$currency";
                    break;
                default:
                    break;
            }

            if (Cache::has($ordersKey)) {
                $orders = Cache::get($ordersKey);
                $book['orders'] = [
                    'buy' => array_slice($orders['buy'] ?? [], 0, random_int(0, 1)),
                    'sell' => array_slice($orders['sell'] ?? [], 0, random_int(0, 1))
                ];
            }
            if (Cache::has($tradesKey)) {
                $book['trades'] = Cache::get($tradesKey);
            }

            if (isset($book)) {
                if ($user->account_status !== 'can_trade_weekly'
                    || !in_array(Carbon::now()->dayOfWeek, ['0', '6'], false)) {
                    $updates['depth'] = $book;
                }
            }

        } catch (Exception $e) {
            $updates['error'] = $e->getMessage();
            $this->timeLine('Error: ' . $e->getMessage());
        }

        return $updates;
    }

    public function justUpdates(WebsocketController $controller, array $prev): array
    {
        return $this->check($controller, $prev);
    }
}
