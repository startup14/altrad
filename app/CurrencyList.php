<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrencyList extends Model
{
    protected $fillable = [
      'type', 'name', 'slug', 'active', 'status'
    ];

    public function scopeStocks($query) {
        return $query->where('type', 'stocks');
    }

    public function scopeCrypto($query) {
        return $query->where('type', 'crypto');
    }
}
