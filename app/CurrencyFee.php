<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrencyFee extends Model
{
    protected $fillable = [
        'currency',
        'fee',
        'market',
        'user_id'
    ];

    protected $table = 'currency_fee';
}
