<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradingBotImage extends Model
{
    protected $fillable = [
        'bot_id',
        'image'
    ];
}
