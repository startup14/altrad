<?php

namespace App\Console\Tasks;

use Illuminate\Log\Logger;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

abstract class BaseTask
{
    private function isLocalOrDevDeploy(): bool
    {
        return in_array(App::environment(), ['local', 'dev', 'develop']);
    }

    protected function timeLine(string $line): void
    {
        if ($this->isLocalOrDevDeploy()) {
            $output = sprintf('[%s] %s.%s', date('Y-m-d H:i:s'), $line, PHP_EOL);
            echo $output;
        }
    }

    protected function notifyAllLogs(string $message): void
    {
        Log::error($message);
        Logger::create(['description' => $message]);
    }
}
