<?php

namespace App\Http\Middleware;

use Closure;

class CheckAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headerAuth = $request->header('Authorization');
        if ($headerAuth == 'Bearer null') {
            return 'Unauthorized';
        } elseif (!auth()->check()) {
            return 'Unauthorized';
        }

        return $next($request);
    }
}
