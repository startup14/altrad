<?php

namespace App\QuoteProviders\Polygon;

use Illuminate\Support\Facades\Cache;

use React\EventLoop;
use Ratchet\Client as Ratchet;

use Ratchet\RFC6455\Messaging;

use Ratchet\RFC6455\Messaging\FrameInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;

use App\QuoteProviders\BaseWebSocket;

class WebSocket extends BaseWebSocket
{
    /**
     * @var string
     */
    protected string $endpoint = "wss://socket.polygon.io/forex";

    /**
     * @var string
     */
    protected string $stream;

    /**
     * @var array
     */
    private array $store;

    /**
     * @var array
     */
    private array $pairs;

    public function __construct(array &$template, callable $onMessage)
    {
        $subscribes = [
            [
                'action' => 'auth',
                'params' => env('POLYGON_API_KEY'),
            ]
        ];
        foreach (array_keys($template) as $symbol) {
            //$subscribes[] = [
            //    'action' => 'subscribe',
            //    'params' => "C.{$symbol}/USD"
            //];
            $subscribes[] = [
                'action' => 'subscribe',
                'params' => "CA.{$symbol}/USD"
            ];
        }

        parent::__construct(function (array $message) use ($onMessage) {
            $answer = $this->answerDecoration($message[0]);
            $onMessage($answer);
        }, $subscribes);
    }

    private function answerDecoration(array $message = []): array
    {
        $answer = [
            'event' => $message['ev'],
        ];
        switch ($message['ev']) {
            case 'C':
                $symbol = explode('/', $message['p'])[0];

                $answer['symbol'] = $symbol[0];
                $answer['ask'] = $message['a'];
                $answer['bid'] = $message['b'];
                $answer['timestamp'] = $message['t'];
                break;
            case 'CA':
                $symbol = explode('/', $message['pair']);

                $answer['symbol'] = $symbol[0];
                $answer['open'] = $message['o'];
                $answer['close'] = $message['c'];
                $answer['high'] = $message['h'];
                $answer['low'] = $message['l'];
                $answer['vol'] = $message['v'];
                $answer['start'] = $message['s'];
                $answer['end'] = $message['e'];
                break;
            default:
                break;
        }
        return $answer;
    }
}
