<?php

namespace App\Console\DataProviders;

use App\User;

final class UserSettings extends BaseUserDataProvider implements IUserDataProvider
{
    private function needRefresh(int $user_id): bool
    {
        $updated_at = User::select('updated_at')->whereId($user_id)->pluck('updated_at')->first();
        return $this->aged() > $updated_at;
    }

    public function fetch(int $user_id): array
    {

        if (! $this->needRefresh($user_id)) {
            return [];
        }

        $updates = [];

        $user = User::select(
            'id',
            'crypto_fee',
            'stock_fee',
            'commodities_fee',
            'positions_fee',
            'balance',
            'account_status',
            'is_active',
            'min_deal',
            'min_deal_stock',
            'min_deal_commodities'
            )->whereId($user_id)->first();

        if ($user) {
            $fees = [
                'crypto_fee' => $user->crypto_fee,
                'stock_fee' => $user->stock_fee,
                'commodities_fee' => $user->commodities_fee,
                'positions_fee' => $user->positions_fee
            ];

            $updates = [
                'fees' => $fees,
                'balance' => $user->balance,
                'account_status' => $user->account_status,
                'is_active' => $user->is_active,
                'min_deal' => $user->min_deal,
                'min_deal_stock' => $user->min_deal_stock,
                'min_deal_commodities' => $user->min_deal_commodities
            ];
        }

        return $updates;
    }
}
