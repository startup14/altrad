<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PammDeposit extends Model
{
    protected $fillable = [
        'pamm_id',
        'user_id',
        'amount'
    ];

    protected $table = 'pamm_deposits';
}
