<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Contracts\CryptoProviderInterface;
use App\Contracts\StocksProviderInterface;
use App\Contracts\CommoditiesProviderInterface;

use App\QuoteProviders\TwelveData\TwelveDataAPI;
use App\QuoteProviders\Binance\BinanceAPI;
use App\QuoteProviders\Polygon\PolygonAPI;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CryptoProviderInterface::class, BinanceAPI::class);
        $this->app->bind(StocksProviderInterface::class, TwelveDataAPI::class);
        $this->app->bind(CommoditiesProviderInterface::class, PolygonAPI::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
