<?php

namespace App\Console\Tasks;

use App\Http\Controllers\WebsocketController;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LogoutSender extends BaseTask implements IBaseTask
{

    public function finish($controller): array
    {
        $updates = [];

        $id = $controller['user_id'];
        $is_active = WebsocketController::USER_OFFLINE;
        $last_updated = Carbon::now()->subSeconds(WebsocketController::USER_FRESH_SEC)->setTimezone('UTC');

        $ghost = User::select(DB::raw("'logout' as logout"))
            ->where(compact('id', 'is_active'))
            ->where('updated_at', '>=', $last_updated)
            ->orderByDesc('updated_at')->first();
        if ($ghost) {
            $updates = $ghost->toArray();
        }

        return $updates;
    }
}
