<?php

namespace App\Console\Commands;


final class UserHop extends AbstractUpdates
{
    protected $signature = 'user:hop {user_id?}';

    protected $description = 'Updates orders, history, positions in Redis';

    public function __construct()
    {
        $this->keyspace = REDIS_KEYSPACE_USER_HOP;
        parent::__construct();
    }

    protected function clientUpdatesFilter(int $user_id): array
    {
        return [];
    }

    protected function clientUpdates(int $user_id, array $cache = []): array
    {
        return (new \App\Console\DataProviders\UserHop())->fetch($user_id);
    }

    protected function needPrecache(): bool
    {
        return false;
    }

    protected function updatesInCache(): array
    {
        return [];
    }
}
