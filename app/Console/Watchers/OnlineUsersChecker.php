<?php

namespace App\Console\Watchers;

use App\Http\Controllers\WebsocketController;
use App\User;

final class OnlineUsersChecker implements ChangeChecker
{

    public function check(WebsocketController $controller, array $prev = []): array
    {
        $onlineIds = User::select('id')->whereOnline('1')->get()->pluck('id');
        $devicesIds = [];
        $realOnlineIds = [];

        foreach ($controller->clients as $client) {
            $devicesIds[] = $controller->clients[$client]['user_id'];
        }

        foreach ($onlineIds as $id) {
            if (!in_array($id, $devicesIds, true)) {
                User::where('id', $id)->update([
                    'online' => '0'
                ]);
                continue;
            }
            $realOnlineIds[] = $id;
        }

        return $realOnlineIds;
    }

    public function justUpdates(WebsocketController $controller, array $prev): array
    {
        return $this->check($controller, $prev);
    }
}
