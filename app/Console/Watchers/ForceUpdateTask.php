<?php

namespace App\Console\Watchers;

use App\Console\Tasks\BaseTask;
use App\Console\Tasks\IBaseTask;
use App\Http\Controllers\WebsocketController;
use Illuminate\Support\Facades\Cache;

class ForceUpdateTask extends BaseTask implements IBaseTask
{
    public function finish($controller): array
    {

        $force_update = Cache::get('force-update');
        $task = [];

        if ($force_update) {
            $task['refresh'] = 'refresh';
            Cache::put('force-update', false);
        }

        return $task;
    }
}
