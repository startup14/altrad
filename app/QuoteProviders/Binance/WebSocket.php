<?php

namespace App\QuoteProviders\Binance;

use Illuminate\Support\Facades\Cache;

use React\EventLoop;
use Ratchet\Client as Ratchet;

use Ratchet\RFC6455\Messaging;

use Ratchet\RFC6455\Messaging\FrameInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;

use App\QuoteProviders\BaseWebSocket;

class WebSocket extends BaseWebSocket
{
    /**
     * @var string
     */
    protected string $endpoint = "wss://stream.binance.com:9443/stream?streams=";

    /**
     * @var string
     */
    protected string $stream;

    /**
     * @var array
     */
    private array $store;

    /**
     * @var array
     */
    private array $pairs;

    public function __construct(array &$store, callable $onMessage)
    {
        foreach ($store as $baseSymbol => &$base) {
            foreach (array_keys($base['quotes']) as $quoteSymbol) {
                $graph = "{$baseSymbol}{$quoteSymbol}";
                $this->pairs[$graph] = [
                    'quote' => $quoteSymbol,
                    'base' => $baseSymbol,
                ];
            }
        }

        $stream = array_map(function ($graph) {
            return strtolower($graph) . "@miniTicker";
        }, array_keys($this->pairs));

        $this->stream = implode('/', $stream);
        $this->endpoint .= $this->stream;
        $this->store = &$store;

        parent::__construct(function (array $message) use ($onMessage) {
            $message = $message['data'] ?? [];
            if (isset($message['s']) && isset($message['e'])) {
                if ($message['e'] === '24hrMiniTicker') {
                    $answer = $this->answerDecoration($message);
                    $onMessage($answer);
                }
            }
        });
    }

    private function answerDecoration(array $message): array
    {

        return [
            'quote' => $this->pairs[$message['s']]['quote'],
            'base'  => $this->pairs[$message['s']]['base'],
            'open'  => (DOUBLE)$message['o'],
            'close' => (DOUBLE)$message['c'],
            'high'  => (DOUBLE)$message['h'],
            'low'   => (DOUBLE)$message['l'],
            'vol'   => (DOUBLE)$message['v'],
        ];
    }
}
